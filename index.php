<?
require_once('animal.php');
$sheep = new Animal("shaun");

echo "Name: ".$sheep->name . "<br>"; // "shaun"
echo "Legs: ".$sheep->legs ."<br>"; // 4
echo "Cold Blooded: ".$sheep->cold_blooded ."<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$frog = new Frog("Buduk");

echo "Name: ".$frog->name . "<br>"; // "shaun"
echo "Legs: ".$frog->legs ."<br>"; // 4
echo "Cold Blooded: ".$frog->cold_blooded ."<br>"; // "no"
$kodok->jump() ;

$ape = new Ape("Kera Sakti");

echo "Name: ".$ape->name . "<br>"; // "shaun"
echo "Legs: ".$ape->legs ."<br>"; // 4
echo "Cold Blooded: ".$ape->cold_blooded ."<br>"; // "no"
$kodok->yell() ;
